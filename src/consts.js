export const ROLE = {
  SUPER_ADMIN: 1,
  USER: 2,
  GUEST: 3,
};

export const imagePath = "http://localhost:8000/storage/images/products/";

export const orderStatus = {
  PENDING: 1,
  APPROVE: 2,
  CANCEL: 3,
  FINISH: 4,
};
