export default [
  {
    path: "/orders",
    name: "Orders",
    component: () =>
      import(/* webpackChunkName: "order" */ "../views/Orders/Orders.vue"),
  },
  {
    path: "/orders/:id",
    name: "OrderDetails",
    component: () => import("../views/Orders/OrderDetails.vue"),
  },
];
