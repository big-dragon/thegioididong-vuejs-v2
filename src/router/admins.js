// Import guards
import * as guards from "../router/guards/guards";

export default [
  {
    path: "/admins/products/create",
    name: "AdminCreateProduct",
    component: () => import("../views/Admins/CreateProduct"),
    beforeEnter: guards.checkIsSuperAdminRole,
  },

  {
    path: "/admins/dashboard",
    name: "AdminDashboard",
    component: () => import("../views/Admins/HomePage/DashBoard"),
    beforeEnter: guards.checkIsSuperAdminRole,
  },

  {
    path: "/admins/orders",
    name: "AdminOrderManager",
    component: () => import("../views/Admins/Orders/ManagerOrders"),
    beforeEnter: guards.checkIsSuperAdminRole,
  },
];
