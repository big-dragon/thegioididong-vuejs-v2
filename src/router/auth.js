import Register from "../views/Register";
import Login from "@/views/Login.vue";

// Import guards
import * as guards from "../router/guards/guards";

export default [
  {
    path: "/register",
    name: "Register",
    component: Register,
    beforeEnter: guards.checkLogin,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    beforeEnter: guards.checkLogin,
  },
  
];
