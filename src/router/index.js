import Vue from "vue";
import VueRouter from "vue-router";
import NotFound from "@/views/Errors/NotFound.vue";

// Import modules router
import ADMIN_ROUTER from "../router/admins";
import AUTH_ROUTER from "../router/auth";
import PRODUCT_ROUTER from "../router/products";
import CARTS_ROUTER from "../router/carts";
import VOUCHER_ROUTER from "../router/vouchers";
import ORDER_ROUTER from "../router/orders";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home"),
  },
  {
    path: "/about",
    name: "About",

    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/About.vue"),
  },
  // 404
  {
    // will match everything
    path: "*",
    name: "NotFound",
    component: NotFound,
  },
  ...ADMIN_ROUTER,
  ...AUTH_ROUTER,
  ...PRODUCT_ROUTER,
  ...CARTS_ROUTER,
  ...VOUCHER_ROUTER,
  ...ORDER_ROUTER,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
