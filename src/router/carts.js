// Import component
import Carts from "@/views/Carts/Carts.vue";
import Checkouts from "@/views/Carts/Checkouts.vue";

// Import guards
import * as guards from "../router/guards/guards";

export default [
  {
    path: "/carts",
    name: "Carts",
    component: Carts,
  },

  {
    path: "/carts/checkout",
    name: "Checkouts",
    component: Checkouts,
    beforeEnter: guards.checkEmtpyCart,
  },
];
