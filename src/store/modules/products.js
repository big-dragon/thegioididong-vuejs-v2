import Api from "../../apis/Api";
import * as types from "../mutation-types/product-types";

const productsModule = {
  namespaced: true,

  state: {
    createProduct: {
      errors: [],
    },

    productColors: [], // array object to store product color quantity to laravel
  },

  getters: {},

  actions: {
    getProductDetails({ commit }, id) {
      return Api().get(`/products/${id}`);
    },

    getCategories() {
      return Api().get("/categories");
    },

    getBrands() {
      return Api().get("/brands");
    },

    getStorageProduct() {
      return Api().get("/storages");
    },

    updateResponseErrors({ commit }, errors) {
      commit(types.UPDATE_ERROR_RESPONSE, errors);
    },

    // admin create product color
    handleUpdateProductColor({ commit }, object) {
      commit(types.UPDATE_PRODUCT_COLOR, object);
    },

    // reset state product color
    resetProductColorState({ commit }) {
      commit(types.RESET_PRODUCT_COLOR);
    },
  },

  mutations: {
    [types.UPDATE_ERROR_RESPONSE](state, errors) {
      state.createProduct.errors = errors;
    },

    [types.UPDATE_PRODUCT_COLOR](state, object) {
      let color_id = object.color_id;
      const record = state.productColors.find(
        (item) => item.color_id === color_id
      );
      console.log(record);
      if (!record) {
        state.productColors.push(object);
      } else {
        Object.assign(record, object);
      }
    },

    [types.RESET_PRODUCT_COLOR](state) {
      state.productColors = [];
    },
  },
};

export default productsModule;
